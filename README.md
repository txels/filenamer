# Filenamer

A CLI to simplify bulk operations with filenames.

The vision is that it should be able to perform a number of file name manipulations on multiple files at a time:
 - Add a prefix/suffix
 - Normalize filenames (e.g. `_` to `-`)
 - Replace a prefix/suffix (e.g. `invoice_` to `linode-2020-`
 - Normalize/modify extensions (e.g. `.yml` to `.yaml` or no extension to `.sh`)

## Usage

The intended `filenamer` usage is by piping it a list of files. It will spit out commands that you can then pipe into a shell.

Filenamer takes two arguments, a `find` (which can be a simple string or an extended regular expression) and a `replace`, plus a few optional flags.

E.g.

    ls testdata/*.pdf | filenamer -c cp '$' '.bak' | sh
    # will create a backup copy of all PDF files in testdata with suffix .bak

## Examples / recipes

Add a prefix `2020-` to all `.txt` files in the current directory (if not piped to `sh`, this will just preview the commands to be run:

    $ ls *.txt | filenamer '^' '2020-'
    mv one-two.txt 2020-one-two.txt
    mv one.txt 2020-one.txt

Normalize extension for all `jpg` files under `~/Pictures` to `jpeg`:

    $ find ~/Pictures/ -name '*.jpg' | filenamer '\.jpg$' '.jpeg' | sh

You can use regex match groups - use parenthesis in the `find` expression to create a group, and refer to it later in the `replace` expression with `$1`. E.g. add a suffix `.old` to the name of all PDFs modified more than one year ago:

    $ find . -mtime +365 | filenamer '(.*)\.pdf$' '$1.old.pdf' | sh

Change prefix from `yyyymmdd` to `yyyy-mm-dd`:

    $ ls | filenamer '^(\d{4})(\d{2})(\d{2})' '$1-$2-$3'
    mv 20200217_125527.jpg 2020-02-17_125527.jpg
    mv 20200217_125550.jpg 2020-02-17_125550.jpg

The same as above, more self-explanatory with named capture groups:

    filenamer '^(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})_' '${year}-${month}-${day}_'

## Potential future extensions

I am yet not sure where I will take this tool further. It could be a number of different directions.

Make it more self-contained, by making it also possible to not depend on piping:
 - Pass it a glob pattern so that it can fetch its own files
 - Being able to perform file operations directly (and not just having to pipe them to a shell)

Based on file content:
 - Fix extensions according to shebangs
 - Find extension inconsistencies/mismatches
