use filenamer::{Args, Filenamer};

/// Search for a pattern in a (filename) and replace it with another pattern

/// E.g. find = '^hello' replace = '2022-01-goodbye'
/// Inputs: "hello-pete.pdf"
/// Output: "2022-01-goodbye-pete.pdf"
/// Output (include input): "hello-pete.pdf 2022-01-goodbye-pete.pdf"

fn main() {
    let args = Args::new();
    let cli = Filenamer::new(args);
    cli.run()
}
