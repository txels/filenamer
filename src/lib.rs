use clap::Parser;
use fancy_regex::Regex;
use std::io;

#[derive(Debug, Parser)]
pub struct Args {
    /// The pattern to look for
    find: String,
    /// The replacement pattern
    replace: String,
    /// Command to prepend to file matched and replacement
    #[clap(short, long, default_value = "mv")]
    command: String,
    /// Flag to exclude input from output
    #[clap(short, long)]
    exclude: bool,
}

impl Args {
    pub fn new() -> Self {
        Args::parse()
    }
}

#[derive(Debug)]
pub struct Filenamer {
    args: Args,
}

impl Filenamer {
    pub fn new(args: Args) -> Self {
        Filenamer { args: args }
    }

    pub fn run(&self) {
        let mut buff = String::new();
        while io::stdin().read_line(&mut buff).expect("error") > 0 {
            match self.process(&buff) {
                Some(out) => println!("{}", out),
                None => (),
            }
            buff.clear();
        }
    }

    fn process(&self, inp: &str) -> Option<String> {
        let out: Option<String>;
        let line = inp.trim();
        let res = self.replace(line);
        if self.args.exclude {
            out = Some(format!("{}", res));
        } else {
            if &res != line {
                out = Some(format!("{} {} {}", &self.args.command, line, res));
            } else {
                out = None;
            }
        }
        out
    }

    fn replace(&self, inp: &str) -> String {
        let re = Regex::new(&self.args.find).unwrap();
        // let replace = inp.replace(&self.args.find, &self.args.replace);
        let replaced = re.replace(inp, &self.args.replace);
        replaced.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn init_cli(find: &str, replace: &str, cmd: &str) -> Filenamer {
        let args = Args {
            find: find.to_string(),
            replace: replace.to_string(),
            command: cmd.to_string(),
            exclude: false,
        };
        Filenamer::new(args)
    }

    #[test]
    fn replace_success() {
        let cli = init_cli("file-", "2022-", "mv");

        assert_eq!(cli.replace("file-name.txt"), "2022-name.txt");
    }

    #[test]
    fn replace_regex_capture_numbered_success() {
        let cli = init_cli("([a-z]*)", "2022-$1-$1", "mv");

        assert_eq!(cli.replace("file-name.txt"), "2022-file-file-name.txt");
    }

    #[test]
    fn replace_regex_capture_named_success() {
        let cli = init_cli(
            "^(?P<year>\\d{4})(?P<month>\\d{2})(?P<day>\\d{2})_",
            "${year}-${month}-${day}_",
            "mv",
        );

        assert_eq!(cli.replace("20210212_file.txt"), "2021-02-12_file.txt");
    }
    #[test]
    fn replace_regex_capture_lookahead_success() {
        let cli = init_cli("(?P<suffix>-[a-z]*)(?=\\.)", "$suffix$suffix-2022", "mv");

        assert_eq!(
            cli.replace("file-name-cool.txt"),
            "file-name-cool-cool-2022.txt"
        );
    }

    #[test]
    fn replace_miss() {
        let cli = init_cli("file-", "2022-", "mv");

        assert_eq!(cli.replace("no-name.txt"), "no-name.txt");
    }

    #[test]
    fn process_success() {
        let cli = init_cli("file-", "2022-", "mv");

        let result = cli.process("file-name.txt").unwrap();

        assert_eq!(&result, "mv file-name.txt 2022-name.txt");
    }

    #[test]
    fn process_miss() {
        let cli = init_cli("file-", "2022-", "mv");

        let result = cli.process("some-name.txt");

        assert_eq!(result, None);
    }

    #[test]
    fn process_regex_success() {
        let cli = init_cli("(^file-)", "2022-$1", "cp");

        let result = cli.process("file-name.txt").unwrap();

        assert_eq!(&result, "cp file-name.txt 2022-file-name.txt");
    }
}
